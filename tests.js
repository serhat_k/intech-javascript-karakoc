require("./Chrono.js");
require("./Game.js");
require("./GestionBlock.js");
require("./Plan.js");

function tests()
{
    /**
     * deux soucis actuellement pose de drapeau pas bon et 
     * soucis dans l'algo de découverte de case vide ... lorsque j'essaie de vérifier pour chaque 
     * block le contenu de la case du dessous et du dessus j'ai un récursive too much ... donc pas de vérif
     * algo pas fiable
     * 
     * test unitaire sur chaque fonction métier :
     * 1 - creation de chaque classe permettant de faire fonctionner le démineur
     * 2 - génération du plan (table de jeu)
     * 3 - test de victoire et d'echec
     * 4 - 
     */
    console.assert(() => {new Chrono(demineur, chrono_display, chrono_pause_button)}, "erreur class create")
    console.assert(() => {new Plan(demineur)}, "erreur class create")
    console.assert(() => {new GestionBlock(plan)}, "erreur class create")
    /* diff and qtebom by default in 10 and 10000 */
    console.assert(() => {new Game(plan,chrono,gestionBlock, 10, 10000, rejouerEl, statut)}, "erreur class create")

    // /** TEST GAME CLASS */
    // console.assert(game.start(), "fail to launch game ...");
    // console.assert(game.raz_demineur(false), "failed to set default value to game class")
    // // if it is reboot of game 
    // console.assert(game.raz_demineur(true), "failed to reboot game")

    // /** CHECK WIN */
    // console.assert(game.checkWin(), "failed to check win ...")

    // /** TEST PLAN CLASS */
    // console.assert(plan.createPlan)
    // console.assert(game.gameover(), "failed to set gameover state ... ");
    // console.assert(game.win(), "failed to set win state");


}
tests();