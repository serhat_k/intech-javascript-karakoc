/**
 * Classe Chrono
 * permet de gérer le chrono via l'initialisation,
 * le démarrage et l'extinction 
 * @param {element} plan
 * @param {element} display
 * @param {element} button 
 */

class Chrono{
    #id_callback
    constructor(plan, display, button)
    {
        this.plan = plan
        this.display = display
        this.button = button
    }

    init_chrono(){
        this.plan.addEventListener('click', function(){
            if(!start && !end){
                alert("C'est parti !");
                start = !start;
                chrono.start_chrono()
            }
        });
    }

    start_chrono()
    {
        this.#id_callback = setInterval(start_chrono_dom, 1000);
    }   

    stop_chrono()
    {
        clearInterval(this.#id_callback)   
    }

    reset_chrono()
    {
        reset_chrono_dom()
    }

}
