/**
 * Classe GestionBlock 
 * permet la gestion des blocks sur le plan
 * modification de contenu, getter ...
 * ou vérification de block par rapport au plan
 * @param {plan} plan
 */

class GestionBlock
{
    
    distanceHaut = 0
    distanceBas = 0
    constructor(plan)
    {
        this.plan = plan
        // this.planElement = plan.planElement
        // this.tableauLigne = plan.tableauLigne
    }

    returnRandomContenu(qteBomb)
    {
        let contenu = ""
        let contenuInt = Math.floor(Math.random() * qteBomb);
        if(contenuInt <= 1) contenu = "bombe"; 
        else contenu = "vide";

        return contenu
    }

    returnContenuBlock(numLigne, numBlock)
    {
        return this.plan.tableauLigne[numLigne][numBlock]
    }

    updateBombeNumber()
    {
        this.plan.tableauBombeElement.forEach(bombeElement => {
            let numLigne =     returnLigneIdOfBlock(bombeElement)
            let numBlock =     returnBlockIdOfBlock(bombeElement)
            this.updateAdjacentBombe(numLigne, numBlock);
        });
    }

    updateAdjacentBombe(numLigne, numBlock)
    {
        // //console.log("updateAdjacentNumer")
        let droite = true
        let gauche = true
        let haut = true
        let bas = true
        // //console.log(numLigne)
        if(numLigne == 0) haut = false;
        if(numLigne == diff-1 ) bas = false;
        if(numBlock == 0) gauche = false;
        if(numBlock == diff-1) droite = false;
        //console.log (bas + '  ' + haut+ '  ' + gauche + '  ' + droite)
        this.incrementBlockAdjacentBombe(droite,gauche,haut, bas, numLigne, numBlock)
    }


    incrementInTableBlockAdjacentBombe(numLigne, numBlock)
    {
        if (this.plan.tableauLigne[numLigne][numBlock] == "vide") this.plan.tableauLigne[numLigne][numBlock] = 1
        else this.plan.tableauLigne[numLigne][numBlock]++
    }



    incrementBlockAdjacentBombe(droite, gauche, haut, bas, numLigne, numBlock)
    {

        if(haut)
        {
            if(!this.itisabomb(numLigne - 1, numBlock))this.incrementInTableBlockAdjacentBombe( numLigne -1, numBlock)
        }

        if(bas){
            
            if(!this.itisabomb(numLigne + 1, numBlock))this.incrementInTableBlockAdjacentBombe( numLigne+1, numBlock)
        }

        if(droite)
        {
            if(!this.itisabomb(numLigne, numBlock + 1 ))this.incrementInTableBlockAdjacentBombe( numLigne, numBlock+1)

            if(bas)
            {
                if(!this.itisabomb(numLigne + 1, numBlock + 1))this.incrementInTableBlockAdjacentBombe( numLigne+1, numBlock+1)
            }

            if(haut)
            {
                if(!this.itisabomb(numLigne - 1, numBlock + 1))this.incrementInTableBlockAdjacentBombe( numLigne-1, numBlock+1)
            }
        }

        if(gauche)
        {
            if(!this.itisabomb(numLigne, numBlock - 1 ))this.incrementInTableBlockAdjacentBombe( numLigne, numBlock-1)

            if(bas)
            {
                if(!this.itisabomb(numLigne + 1, numBlock - 1))this.incrementInTableBlockAdjacentBombe( numLigne+1, numBlock-1)
            }

            if(haut)
            {
                if(!this.itisabomb(numLigne - 1, numBlock - 1))this.incrementInTableBlockAdjacentBombe( numLigne-1, numBlock-1)
            } 
        }
    }
        
    itisabomb(numLigne, numBlock)
    {
        if (this.returnContenuBlock(numLigne, numBlock) == "bombe") return true
        else return false
    }
        
    itisaBombAdjacent(numLigne, numBlock)
    {
        if((numLigne >= 0 && numLigne < diff) && (numBlock >= 0 && numBlock < diff))
        {
            let contenu = this.returnContenuBlock(numLigne, numBlock)
            if (contenu != "bombe" && contenu != "vide" && contenu > 0) return true
            else return false
        }
        return true
    }


    itisnotabomb_and_notbombadjacent(numLigne, numBlock)
    {
        if(!this.itisabomb(numLigne, numBlock) && !this.itisaBombAdjacent(numLigne, numBlock))
        {
            return true; 
        }
        else
        {
            return false;   
        }
    }

    topIsEmpty(numLigne, numBlock)
    {
        if(numLigne == 0)
        {
            return false
        }

        //blockActuelle = document.querySelector("#ligne" + numLigne - 1).querySelector("#block" + indexBlock).removeAttribute('class');
        if(this.itisnotabomb_and_notbombadjacent(numLigne, numBlock)) return true;
        else return false
    }   

    bottomIsEmpty(numLigne, numBlock)
    {

        if(numLigne == diff - 1)
        {
            return false
        }

        //blockActuelle = document.querySelector("#ligne" + numLigne - 1).querySelector("#block" + indexBlock).removeAttribute('class');
        if(this.itisnotabomb_and_notbombadjacent(numLigne, numBlock)) return true;
        else return false
    }


    itis_end(constante, numBlock, compteur, numLigne = 0)
    {
        switch(constante)
        {
            case 1 : if(numBlock+compteur == diff) return false
            case 2 : if(numBlock-compteur == 0) return false
            case 3 : if(numLigne-distanceHaut == 0) return false
            case 4 : if(numLigne+distanceBas == diff) return false

        }

        return true
    }


    discoverEmpty(numLigne, numBlock)
    {
        // this.distanceHaut = numLigne;
        // this.distanceBas = diff - (this.distanceHaut + numLigne);

        for(let distance_haut_index = numLigne; distance_haut_index >= 0; distance_haut_index--)
        {
            if(!this.itisaBombAdjacent(distance_haut_index, numBlock) || !this.itisabomb(distance_haut_index, numBlock))
                this.displayEmptyBlockRightLeft(distance_haut_index, numBlock)
        }

        for(let distance_bas_index = numLigne; distance_bas_index < diff; distance_bas_index++)
        {
            if(!this.itisaBombAdjacent(distance_bas_index, numBlock) || !this.itisabomb(distance_bas_index, numBlock))
            this.displayEmptyBlockRightLeft(distance_bas_index, numBlock)
        }
    }

    displayContenuSpecificBlock(numLigne, numBlock)
    {
        let contenu = this.returnContenuBlock(numLigne, numBlock)

        console.log("specific : " + contenu)
        if(contenu !="bombe" && contenu != "vide")
        {
            setContentOfBlock(numLigne, numBlock, contenu)
        }

    }

    displayEmptyBlockRightLeft(numLigne, numBlock)
    {
        let compteur_right = this.returnNumberOfEmptyBlock(true, numLigne, numBlock);
        let left_compteur = this.returnNumberOfEmptyBlock(false, numLigne, numBlock)
        // compteur_bas = 
        // compteur haut = 
        //right

        // if(!this.itis_end(1, numBlock, compteur_right))
        // {
        //     displayBlock(numLigne, numBlock+compteur_right-1)
        // }

        // //left
        // if(!this.itis_end(2, numBlock, left_compteur))
        // {
        //     displayBlock(numLigne, numBlock-left_compteur)
        // }

        // //top
        // if(!this.itis_end(3, numBlock, left_compteur, numLigne))
        // {

        // }

        // //bottom
        // if(!this.itis_end(4, numBlock, left_compteur, numLigne))
        // {
        // }

        
        console.log("right = " + compteur_right);
        // ligneEnCours = document.querySelector("#ligne" + numLigne)
        // blockDeLaLigne = document.querySelector("#ligne" + numLigne).querySelector("#block" + numBlock)
        // console.log(compteur)
    }


    returnNumberOfEmptyBlock(constante, numLigne, numBlock)
    {
        let compteur = 0;
        if(constante == 1)
        {
            for(let indexBlock = numBlock; !this.itisabomb(numLigne, indexBlock) && indexBlock < diff; indexBlock++)
            {

                blockOpen(numLigne, numBlock)
                displayBlock(numLigne, indexBlock)
                if(this.topIsEmpty(numLigne, indexBlock))
                {
                    //this.discoverEmpty(numLigne-1, indexBlock)

                    //returnNumberOfEmptyBlock(1,numLigne, indexBlock)
                }
                    // discoverEmpty(numLigne-1, indexBlock)
                    //console.log("topisempty droite")
                if(this.bottomIsEmpty(numLigne, indexBlock))
                {
                    // returnNumberOfEmptyBlock(1,numLigne, indexBlock)
                    //this.discoverEmpty(numLigne+1, indexBlock)

                }
                    //discoverEmpty(numLigne+1, indexBlock)
                    //console.log("bottom is empty droite")
                compteur++
            } 
        }
        else{
            for(let indexBlock = numBlock; !this.itisabomb(numLigne, indexBlock) && indexBlock >= 0; indexBlock--)
            {
                blockOpen(numLigne, numBlock)
                displayBlock(numLigne, indexBlock)
                if(this.topIsEmpty(numLigne, indexBlock))
                {
                    //this.discoverEmpty(numLigne-1, indexBlock)
                    // returnNumberOfEmptyBlock(2, numLigne, indexBlock)
                }
                    // discoverEmpty(numLigne-1, indexBlock)
                    //console.log("topisempty droite")
                if(this.bottomIsEmpty(numLigne, indexBlock))
                {
                    //this.discoverEmpty(numLigne+1, indexBlock)
                    // returnNumberOfEmptyBlock(2,numLigne, indexBlock)
                }

                compteur++
            }
        }
        return compteur-1
    }
}