function start_chrono_dom(){
    secondes = document.querySelector("#secondes").textContent
    minutes = document.querySelector("#minutes").textContent

    if(secondes >= 59)
    {
        document.querySelector("#minutes").textContent++
        document.querySelector("#secondes").textContent = 0

        //document.getElementById("minutes").textContent++;
    }
    else{
        document.querySelector("#secondes").textContent++

        //document.getElementById("secondes").textContent++;
        }
}

function displayBlock(indexLigne, indexColonne)
{
    blockActuelle = document.querySelector("#ligne" + indexLigne).querySelector("#block" + indexColonne)
    contenu = gestionBlock.returnContenuBlock(indexLigne, indexColonne)

    if(contenu == "bombe")
    {
        blockActuelle.textContent = "B"
        blockActuelle.style.background = "red"
    }
    else if(contenu == "vide")
    {
        blockOpen(indexLigne, indexColonne)
        //blockActuelle.style.background = "white";
    }
    else{
        blockOpen(indexLigne, indexColonne)
        blockActuelle.textContent = contenu
    }
}

function returnLigneIdOfBlock(block)
{
    return parseInt(block.parentNode.getAttribute('id').split("ligne")[1]); 
}

function returnBlockIdOfBlock(block)
{
    return parseInt(block.getAttribute('id').split("block")[1]);
}

function blockOpen(numLigne, numBlock)
{
    document.querySelector("#ligne" + numLigne).querySelector("#block" + numBlock).removeAttribute('class');
    document.querySelector("#ligne" + numLigne).querySelector("#block" + numBlock).setAttribute('class', 'block open');
}

function setContentOfBlock(numLigne, numBlock, contenu)
{
    document.querySelector("#ligne" + numLigne).querySelector("#block" + numBlock).textContent = contenu
}

function reset_chrono_dom()
{
    document.querySelector("#secondes").textContent = 0
    document.querySelector("#minutes").textContent = 0
}

function compteurAllOpenBlock()
{
    let compteur = 0;
    for(let indexLigne = 0; indexLigne < diff; indexLigne++)
    {
        for(let indexColonne = 0; indexColonne < diff; indexColonne++)
        {
            blockAttribute = document.querySelector("#ligne" + indexLigne).querySelector("#block" + indexColonne).getAttribute('class');
            if (blockAttribute == "block open")
            {
                compteur++
            }
        }
    }
    return compteur;
}

function reload_diff(game)
{
        diff = parseInt(document.getElementById("diff").value.split(",")[0]) // taille
        qteBomb = parseInt(document.getElementById("diff").value.split(",")[1]) // chance 1/qteBomb de tombé dessus
        console.log (diff + " " + qteBomb)
        game.diff = diff
        game.qteBomb = qteBomb
}