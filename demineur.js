const demineur = document.getElementById("demineur");
const chrono_display = document.getElementById("chrono");
const chrono_pause_button = document.getElementById("pause");
const statut = document.getElementById("status")
const rejouerEl = document.getElementById("rejouer")
const jouer = document.getElementById("start")
const chrono = new Chrono(demineur, chrono_display, chrono_pause_button)
const plan = new Plan(demineur)
const gestionBlock = new GestionBlock(plan)
let diff = parseInt(document.getElementById("diff").value.split(",")[0]) // taille
let qteBomb = parseInt(document.getElementById("diff").value.split(",")[1]) // chance 1/qteBomb de tombé dessus
const game = new Game(plan,chrono,gestionBlock, diff, qteBomb, rejouerEl, statut)



game.start()

let start = false;
let end = false;


