/**
 * à partir de l'element dans lequel sera crée le plan 
 * intégre des fonctions de création de plan...
 * @param {element} planElement
 */
class Plan{
    tableauBombeElement = Array()
    tableauLigne = Array() 

    constructor(planElement)
    {
        this.planElement = planElement
    }


    createPlan(diff, qteBomb)
    {
        for(let ligne = 0; ligne < diff; ligne++){

            let ligneDiv = document.createElement("div")
            ligneDiv.setAttribute('id', 'ligne' + ligne) ;
            ligneDiv.setAttribute('class', 'ligne');

            let tableauBlock = Array()
            for(let colonne = 0; colonne < diff; colonne++ )
            {
                let contenuForThisBlock = gestionBlock.returnRandomContenu(qteBomb);
                let block =  document.createElement("div")
                block.setAttribute('id',"block" + colonne);
                block.setAttribute('class', 'block closed');
                if(contenuForThisBlock === "bombe") 
                {  
                    this.tableauBombeElement.push(block)
                }
                block.addEventListener('click', el => {this.eventBlock(el)})
                ligneDiv.append(block);

                tableauBlock.push(contenuForThisBlock)
            }
            demineur.append(ligneDiv);
            this.tableauLigne.push(tableauBlock)
        }

        return true
    }

    eventBlock(block)
    {
        block = block.srcElement
        if(start)
        {
            let numBlock = block.getAttribute('id').split("block")[1];
            let numLigne = block.parentNode.getAttribute('id').split("ligne")[1];
            let contenu = gestionBlock.returnContenuBlock(parseInt(numLigne), parseInt(numBlock));
            if(contenu == "bombe")
            {
                game.gameover()
            }
            else if (contenu > 0)
            {
                displayBlock(parseInt(numLigne), parseInt(numBlock))
                block.removeAttribute('class')
                block.setAttribute('class', 'block open')
                game.checkWin()

            }
            else
            {
                gestionBlock.discoverEmpty(parseInt(numLigne), parseInt(numBlock))
                game.checkWin()

            }

        }
        return true;
    }
}

