/**
 * Classe Game permettant la gestion du jeu ... 
 * Start, stop, gestion gagné, perdu...
 * @param {Plan} plan
 * @param {Chrono} chrono
 * @param {GestionBlock} gestion
 * @param {int} diff
 * @param {int} qteBomb
 * @param {element} rejouer
 * @param {element} statut
 */
class Game
{
    id_checkWin = 0
    constructor(plan, chrono, gestion, diff, qteBomb, rejouer, statut)
    {
        this.plan = plan
        this.chrono = chrono
        this.gestion = gestion 
        this.diff = diff
        this.qteBomb = qteBomb
        this.rejouerElement = rejouer
        this.statut = statut
    }
    
    raz_demineur(reboot)
    {
        this.plan.planElement.innerHTML = ""
        this.plan.tableauLigne = Array() 
        this.plan.tableauBombeElement = Array()

        this.chrono.stop_chrono()
        if(reboot)
        {
            plan.createPlan(diff, qteBomb)
            this.chrono.reset_chrono()
            this.gestion.updateBombeNumber()
            this.statut.textContent = ""
            end = !end ;
        }
        return true
    }

    start()
    {
        this.rejouerElement.addEventListener('click', function() {
            start = !start;
            end= !end;
            reload_diff(this)
            game.raz_demineur(true)
        });
        
        this.chrono.button.addEventListener('click', function(){
            start = false;
            chrono.stop_chrono()
        });

        this.chrono.init_chrono()
        this.plan.createPlan(this.diff, this.qteBomb)
        this.gestion.updateBombeNumber()
        return true
    }

    checkWin()
    {
        let compteurAllBlock = compteurAllOpenBlock();
        let lenB = this.plan.tableauBombeElement.length
        if(compteurAllBlock == ((diff*diff)-lenB))
        {
            this.win()
        }
        return true

    }

    gameover()
    {
        // numBlock = element.getAttribute('id').split("block")[1];
        // numLigne = element.parentNode.getAttribute('id').split("ligne")[1]
        this.statut.textContent = " Vous avez perdu ! "
        this.chrono.stop_chrono()
        for(let indexLigne = 0; indexLigne < diff; indexLigne++)
        {
            for(let indexColonne = 0; indexColonne < diff; indexColonne++)
            {
               displayBlock(indexLigne, indexColonne)
               contenu = gestionBlock.itisabomb(indexLigne, indexColonne)
               if(!contenu)
               {
                   blockOpen(indexLigne, indexColonne)
               }
            }
        }
        return true

    }

    win()
    {
        this.statut.textContent = " Vous avez gagnez !"
        this.chrono.stop_chrono()
        return true

    }

}